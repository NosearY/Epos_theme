jQuery(document).ready(function($) {
      //call ajax api to init post on the first page.
      initPosts();

      $(window).scroll(function() {
        if ($(this).scrollTop() > 1) {
          console.log('Hello');
        } else {
          $('header').removeClass("sticky");
        }
      });

      particlesJS.load('particles-js', templateUrl + '/js/particlesjs-config.json', function() {
        console.log('callback - particles.js config loaded');
      });

      $('#nav-sidebar li').on('click', function() {
        $(this).toggleClass("active");
      });


      $(document).on('click', '.page-numbers', function(event) {
        event.preventDefault();
        var clickedPage = $(this).text();

        $.ajax({
          url: ajaxpagination.ajaxurl,
          type: 'post',
          data: {
            action: 'ajax_pagination',
            query_vars: ajaxpagination.query_vars,
            page: clickedPage
          },
          success: function(html) {
            console.log(html);
            $('.post-grid').empty();
            $('.post-grid').append(html);
          },
          error: function(error) {
            //alert("error");
          }
        })
      });


      function initPosts() {
        var clickedPage = 1;
        $.ajax({
            url: ajaxpagination.ajaxurl,
            type: 'post',
            data: {
              action: 'ajax_pagination',
              query_vars: ajaxpagination.query_vars,
              page: clickedPage
            },
            success: function(html) {
              console.log(html);
              $('.post-grid').append(html);
            },
            error: function(error) {
              //alert("error");
            }
          });
        }
      });
