<?php

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link href='https://fonts.googleapis.com/css?family=Sanchez' rel='stylesheet' type='text/css'>
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
	<![endif]-->
	<script type="text/javascript">
var templateUrl = '<?= get_bloginfo("template_url"); ?>';
</script>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<nav id="main-page-nav">
		<div class="nav-wrapper">
			<span class="nav-logo">EPOS影音設計實驗室</span>
			<ul class="nav-ul">
				<li><a>履歷</a></li>
				<li><a>作品</a></li>
				<li><a>聯繫</a></li>
			</ul>
	</div>
	</nav>
<div class="container-fluid body-wrapper">
	<div id="particles-js" class="fadein-long"></div>
<!-- <div class="nav-wrapper">

</div> -->

<!-- opeening main body tag -->
