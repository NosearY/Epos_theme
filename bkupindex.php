<?php get_header(); ?>
<!--     <div class="bg-objects">
    <span id="obj1"></span>
    <span id="obj2"></span>
    <span id="obj3"></span>
</div> -->
<!--  <section class="fixed-top">
   <ul class="nav nav-pills">
      <li role="presentation" class="active"><a href="#">Home</a></li>
      <li role="presentation"><a href="#">Profile</a></li>
      <li role="presentation"><a href="#">Messages</a></li>
    </ul>
</section> -->
<div id="left-nav">
    <a href="#29" class="left-nav-a"></a>
    <a href="#24" class="left-nav-a"></a>
    <a href="#20" class="left-nav-a"></a>
    <a href="#8" class="left-nav-a"></a>
</div>

<div class="row">
    <section id="main-container-wrapper" class="col-md-12 col-sm-12 col-xs-12">
        
            <div class="main-content-area">

                        <?php while(have_posts()) : the_post();
                        ?>
                        <div id="<?php the_ID();?>" class="post-wrapper">                        
                            <div class="post-entries">
                                    <div class="post-header">
                                        <div class="post-header-title">
                                            <div class="post-dates" onClick=window.open("<?php the_permalink(); ?>")>
                                                <?php the_date('M d', '<h3>', '</h3>'); ?>
                                            </div>
                                            <div class="post-titles">
                                            <!-- onClick=window.open("<?php the_permalink(); ?>")-->
                                                        <article>
                                                                <!-- <h1><a href="<?php the_permalink(); ?>"><?php the_title();?></a></h1> -->
                                                                <h1><?php the_title();?></h1>
                                                        </artilce>
                                            </div>
                                        </div>
                                        <div class="post-tags">
                                        <?php
                                            $posttags = get_the_tags();
                                            if ($posttags) {
                                              foreach($posttags as $tag) {
                                                echo '<span class="label label-info">' . $tag->name . '</span>'; 
                                              }
                                            }
                                        ?>
                                        </div>
                                    </div>
                                    <div class="post-content well">
                                            <?php the_content();?>
                                    </div>

                            </div>
                        </div>       
                        <?php endwhile;?>
            </div>
        
    </section>
</div>

<?php get_footer(); ?>
