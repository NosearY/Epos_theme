<?php get_header(); ?>      
	 <?php while ( have_posts() ) : the_post();?>
                            <section class="post-entries">
                                    <div class="post-header">
                                        <div class="post-header-title">
                                            <div class="post-dates" onClick=window.open("<?php the_permalink(); ?>")>
                                                <?php the_date('M d', '<h3>', '</h3>'); ?>
                                            </div>
                                            <div class="post-titles">
                                                 <h1><?php the_title();?></h1>
                                            </div>
                                        </div>
                                        <div class="post-tags">
                                        <?php
                                            $posttags = get_the_tags();
                                            if ($posttags) {
                                              foreach($posttags as $tag) {
                                                echo '<span class="label label-info">' . $tag->name . '</span>'; 
                                              }
                                            }
                                        ?>
                                        </div>
                                    </div>
                                    <div class="post-content well">
                                            <?php the_content();?>
                                    </div>
                            </section>
        <?php endwhile?>
<?php get_footer(); ?>