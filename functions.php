<?php

define("CONST_POST_PER_PAGES", 4);

add_action( 'wp_enqueue_scripts', 'enqueue_assets' );
add_theme_support( 'post-thumbnails' );
/*set_post_thumbnail_size( 150, 150 );*/

function enqueue_assets(){
	//equeue scripts
	wp_enqueue_script('jquerymin' , get_template_directory_uri() . '/js/jquery-1.11.3.min.js');
	wp_enqueue_script('bsscript' , get_template_directory_uri() . '/js/bootstrap.min.js');
	wp_enqueue_script('particlejsmin', get_template_directory_uri() . '/js/particles.js');
	wp_enqueue_script('script' , get_template_directory_uri() . '/js/script.js');
	/*wp_enqueue_style('hvrcss',get_template_directory_uri() . '/css/hover-min.css');*/
	//enqueue style sheets
	wp_enqueue_style('bsmincss' , get_template_directory_uri() . '/css/bootstrap.min.css');
	wp_enqueue_style('style' , get_template_directory_uri() . '/style.css');
	wp_enqueue_style('hover' , get_template_directory_uri() . '/css/hover-min.css');
	//embed ajax url into main page
	global $wp_query;
	wp_localize_script( 'script', 'ajaxpagination', array(
		'ajaxurl' => admin_url( 'admin-ajax.php' ),
		'query_vars' => json_encode( $wp_query->query )
	));
}

function custom_post_query_order($query){

	if (! is_admin() && $query->is_main_query()){
		$query->set('posts_per_page',CONST_POST_PER_PAGES );
	}
}

add_action('pre_get_posts','custom_post_query_order');

/*
* Initialize navigation bar
*/
function wp_pagenavi() {

global $wp_query;

$big = 999999999; // need an unlikely integer

$pages = paginate_links( array(
        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
        'format' => '?paged=%#%',
        'current' => min( -1, get_query_var('paged') ),
        'total' => $wp_query->max_num_pages,
        'type'  => 'array',
        'show_all' => true,
        'prev_next' => false
    ) );
    if( is_array( $pages ) ) {
        $paged = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
				echo '<ul>';
        foreach ( $pages as $page ) {
                echo  '<li id="maing-nav" class="fadein">' . $page . '</li>';
        }
				echo '</ul>';
    }
}

/*
* API for ajax pagination call
*/
function ajax_pagination() {
	$html = '';
	//init query variables
	$query_vars = json_decode( stripslashes( $_POST['query_vars'] ), true );
	$query_vars['paged'] = $_POST['page'];
	$query_vars['post_status'] = 'publish';
	$query_vars['posts_per_page'] = CONST_POST_PER_PAGES ;

	$posts = new WP_Query( $query_vars );
	$GLOBALS['wp_query'] = $posts;

	//add_filter( 'editor_max_image_size', 'my_image_size_override' );
	if( ! $posts->have_posts() ) {
        get_template_part( 'content', 'none' );
    }
    else {
        while ( $posts->have_posts() ) {
            $posts->the_post();
						$html.= '<div id = "'.get_the_ID() .'" class="post-thumbnail fadein">';
						$html.= get_the_post_thumbnail(null, 'post-thumbnail');
						$html.= '<div class="post-title">';
						$html.= get_the_title();
						$html.=	'</div>';
						$html.=	'</div>';
        }
    }
    remove_filter( 'editor_max_image_size', 'my_image_size_override' );

	echo $html;
	exit();
}

add_action( 'wp_ajax_ajax_pagination', 'ajax_pagination' );
add_action( 'wp_ajax_nopriv_ajax_pagination', 'ajax_pagination' );

/*
* Ajax pagination
*/
function ajaxPagination(){
	$page = $_GET['page'];
	$html = '';

	if(filter_var(intval($page), FILTER_VALIDATE_INT)){
		$temp_var = $page;

	}

	echo $html;
	exit();
}

//for logged users
add_action( 'wp_ajax_ajax_pagination', 'ajaxPagination' );
//for non-logged users
add_action( 'wp_ajax_nopriv_ajax_pagination', 'ajaxPagination' );
